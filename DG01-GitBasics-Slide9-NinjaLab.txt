# Commands as shown below:

# Set configuration details for user's name and email
git config --global user.name "Your Name"
git config --global user.email "yourname@yourdomain.com"

# Check the settings
git config --list
