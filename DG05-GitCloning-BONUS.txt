# Cloning from an existing remote repository

cd ~
git clone https://edBitBucket@bitbucket.org/edBitBucket/qadevopsgit.git

# Removing current repo details
cd qadevopsgit
rm -r .git
# Press 'y' at each prompt
# Check it is no longer a git repo
git status      # fatal: Not a git repository...

# Make it a git repo
git init

# Create your own BitBucket/GitHub account
# Create a new repository

# Create a local copy of the repo
# Add files to it and commit them locally
# Push to the remote repo
# Refresh your online page and see the commit history
