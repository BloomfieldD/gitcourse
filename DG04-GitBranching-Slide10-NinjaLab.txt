# Ensure that in Windows you are running Git Bash
# Continues in the wipeout repo

git log --oneline
git checkout <SHA>
git log --oneline
touch newfile
git add newfile
git commit -m "newfile added"
git tag where_it_all_began -m "Committing a tag at the start"
git checkout master
gitk                # Examine the GUI
git log --oneline
git tag another_tag <ANOTHER_SHA>
gitk                # Examine the GUI
git show another_tag
git checkout another_tag
