# Ensure that in Windows you are running Git Bash

cd ~
mkdir stashing
cd stashing
git init

vi mywip
# Press 'i' to Insert text
a line of text
# Press ESC then SHIFT + Z + Z to save and exit

git add mywip
git commit -m "mywip added"

echo "another line of text" >> mywip

git add mywip
git commit -m "mywip added"

git log --oneline
git checkout <SHA>

echo "text to add to previous committed file" >> mywip

git checkout master     # Not allowed as changes have been made and not committed

git stash               # Stashes the changes made

git checkout master

git stash list

git checkout <SHA>
git stash pop
echo "more text" >> mywip
git add mywip
git commit -m "more text added to mywip"
git checkout master
